import sys
from copy import deepcopy
import re

import numpy as np

import pjlsa
import pytimber


lsa=pjlsa.LSAClient()
cals=pytimber.LoggingDB()

class LHCRun:
    def __init__(self,year):
        self.year=year
        self.t1=f"{year}-01-01 00:00:00"
        self.t2=f"{year}-12-31 23:59:59"
        self.set_fills()

    def set_fills(self):
        self.fills={}
        fills=lsa.findBeamProcessHistory(self.t1,self.t2,accelerator='lhc')
        for filln,bp_list in fills.items():
            beam_processes=[(ts,bp.split('@')[0]) for ts,bp in bp_list]
            self.fills[filln]=LHCFill(filln,beam_processes)

    def find_beam_processes(self,regexp=''):
        reg=re.compile(regexp)
        out={}
        for filln,fill in self.fills.items():
            for ts,bp in fill.beam_processes:
                res=reg.match(bp)
                if res:
                    out.setdefault(bp,[]).append(filln)
        return out

    def hist_beam_processes(self,regexp=''):
        return list(sorted( (len(v),k) for k,v in self.find_beam_processes(regexp).items()))



class LHCCycle:
    """Sequence of beam process"""

    @classmethod
    def from_file(cls,cycle_fn,year):
        bplist=[bp.strip() for bp in  open(cycle_fn)]
        return cls(bplist,year)

    def __init__(self, beam_processes):
        self.beam_processes=beam_processes

    def get_fills(self,lhcrun):
        def match(fill):
            fillbp=set([bp for ts,bp in fill.beam_processes])
            return all([bp in fillbp for bp in self.beam_processes])
        return sorted([ff.filln for ff in lhcrun.fills.values() if match(ff)])


class LHCFill:
    def __init__(self,filln,beam_processes):
        self.filln=filln
        self.beam_processes=beam_processes

    def get_data(self):
        return cals.getLHCFillData(self.filln)

    def get_start(self):
        return self.beam_processes[0][0]

    def bp_in_fill(self,beam_process):
        for ts,bp in self.beam_processes:
            if beam_process==bp:
                return True
        else:
            return False


class LHCBeamProcess:
    def __init__(self,name):
        self.name=name

    def get_optic_table(self):
        return lsa.getOpticTable(self.name)

    def get_trims(self,params,lhcrun):
        import jpype
        out=[]
        for param in params:
            try:
               trims=lsa.getTrims(param,beamprocess=self.name,start=lhcrun.t1,end=lhcrun.t2)[param]
               for ts,trim in zip(*trims):
                  out.append([ts,param,trim])
            except jpype.JException as ex:
                print("Error extracting parameter '%s': %s" % (param, ex))
            except KeyError as ex:
                print("Empty response for '%s': %s" % (param, ex))
        out.sort()
        return out

    def get_model_history(self,params,lhcrun,presettings):
        optable=lsa.getOpticTable(self.name)

        models=[]
        index=[]
        for op in optable:
            predict={'opname':op.name}
            predict.update(presettings)
            models.append(LHCModel(predict))
            index.append(op.time)

        models= LHCModels(index,models)

        trims=self.get_trims(params,lhcrun)

        for filln,fill in lhcrun.fills.items():
            if fill.bp_in_fill(self.name):
                 trims.append([fill.get_start(),'FILLN',filln])

        trims.sort()

        out={}

        cfill=0
        for ts,setting,val in trims:
            #print(cfill,ts,setting,val)
            if setting=='FILLN':
                out[cfill]=models.copy()
                cfill=val
            else:
                models.apply_trim(setting,val[0],val[1])
        out[cfill]=models.copy()
        return out

class LHCModel:
    def __init__(self,settings):
        self.settings=settings

    def copy(self):
        return LHCModel(self.settings.copy())

    def to_mad(self,knobs):
        out=[]
        for k,v in self.settings.items():
            if k in knobs.lsa:
                out.append(knobs.to_mad(k,v))
        return '\n'.join(out)


class LHCModels:
    def __init__(self,index,models):
        if len(index)!=len(models):
            raise ValueError
        self.index=np.array(index)
        self.models=models

    def apply_trim(self,name,params,values):
        ivalues=np.interp(self.index,params,values)
        for value,model in zip(ivalues,self.models):
            model.settings[name]=value

    def apply_all(self,name,value):
        for model in self.models:
            model.settings[name]=value

    def copy(self):
        return LHCModels(self.index.copy(),[m.copy() for m in self.models])


class LHCKnobs:
    @classmethod
    def from_file(cls,fn):
        lst=[]
        for ln in open(fn):
            try:
                lst.append(LHCKnob(*ln.strip().split(', ')))
            except:
                pass
        return cls(lst)

    def __init__(self,knob_list):
        self.mad={}
        self.lsa={}
        for knob in knob_list:
            self.mad[knob.mad]=knob
            self.lsa[knob.lsa]=knob

    def mad_value(self,lsa_name,lsa_value):
        return  self.lsa[lsa_name].mad_value(lsa_value)

    def lsa_value(self,mad_name,mad_value):
        return  self.mad[mad_name].lsa_value(mad_value)

    def to_mad(self,lsa_name,lsa_value):
        return self.lsa[lsa_name].to_mad(lsa_value)

    def add(self,knob):
        self.mad[knob.mad]=knob
        self.lsa[knob.lsa]=knob

    def add_knob(self,mad,lsa,scaling,test):
        self.add(LHCKnob(mad,lsa,scaling,test))

class LHCKnob:
    def __init__(self,mad,lsa,scaling,test):
        self.mad=mad
        self.lsa=lsa
        self.scaling=float(scaling)
        self.test=float(test)

    def mad_value(self,lsa_value):
        return lsa_value*self.scaling

    def lsa_value(self,mad_value):
        return mad_value/self.scaling

    def to_mad(self,lsa_value):
        return f"{self.mad}={self.mad_value(lsa_value)};"




