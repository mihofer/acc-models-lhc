from lhcmodels import LHCRun, LHCCycle, LHCBeamProcess, LHCKnobs
from lhcmodels import lsa


settings=[
 'LHCBEAM/MOMENTUM',
 'LHCBEAM/IP1-XING-V-MURAD',
 'LHCBEAM/IP2-XING-V-MURAD',
 'LHCBEAM/IP5-XING-H-MURAD',
 'LHCBEAM/IP8-XING-H-MURAD',
 'LHCBEAM/IP1-SEP-H-MM',
 'LHCBEAM/IP2-SEP-H-MM',
 'LHCBEAM/IP5-SEP-V-MM',
 'LHCBEAM/IP8-SEP-V-MM',
 'LHCBEAM/IP2-ANGLE-H-MURAD',
 'LHCBEAM/IP8-ANGLE-V-MURAD',
 'RFBEAM1/TOTAL_VOLTAGE',
 'RFBEAM2/TOTAL_VOLTAGE' ]

knobs=LHCKnobs.from_file('../operation/knobs.txt')
knobs.add_knob("nrj",'LHCBEAM/MOMENTUM',1.0,1)


lhcrun=LHCRun(2018)

lhcrun.hist_beam_processes('PHYSICS')


pp_lumi=LHCCycle(
        ["RAMP_PELP-SQUEEZE-6.5TeV-ATS-1m-2018_V3_V1",
          "QCHANGE-6.5TeV-2018_V1",
          "SQUEEZE-6.5TeV-ATS-1m-30cm-2018_V1",
          "PHYSICS-6.5TeV-30cm-120s-2018_V1"])

ion_lumi=LHCCycle(
        ["RAMP-SQUEEZE-6.37TeV-ATS-Ion-2018_V2",
         "QCHANGE-6.37TeV-Ion-2018_V1",
         "SQUEEZE-6.37TeV-ATS-1m-50cm-2018_ION_V1",
         "PHYSICS-6.37TeV-50cm-240s-Ion-2018_V1"])

ion_lumi_neg=LHCCycle(
        ["PHYSICS-6.37TeV-50cm-240s-Ion-2018_NegPolarity-V1"])

pp_highbeta=LHCCycle(
        ["PHYSICS-6.5TeV-90m-HighB-120s-2018_V1"]
        )


pp_lumi.get_fills(lhcrun)
ion_lumi.get_fills(lhcrun)


bp=LHCBeamProcess("RAMP_PELP-SQUEEZE-6.5TeV-ATS-1m-2018_V3_V1")

presettings={
        'sequence':'lhc.seq',
        'dqx.b1':.05, 'dqy.b1':.05, 'dqx.b2':.05, 'dqy.b2':.05,
        'dqpx.b1':13, 'dqpy.b1':13, 'dqpx.b2':13, 'dqpy.b2':13,
        }

out=bp.get_model_history(settings,lhcrun,presettings)
