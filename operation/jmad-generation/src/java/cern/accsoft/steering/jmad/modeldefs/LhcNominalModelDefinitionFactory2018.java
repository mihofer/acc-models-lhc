/**
 *
 */
package cern.accsoft.steering.jmad.modeldefs;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;
import static cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder.modelFile;

import java.util.ArrayList;
import java.util.List;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.create.AbstractLhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

public class LhcNominalModelDefinitionFactory2018 extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("toolkit/init-constants.madx"));
        modelDefinition.addInitFile(new CallableModelFileImpl("lhc.seq"));
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2018";
    }

    @Override
    protected void addPostUseFiles(RangeDefinitionImpl rangeDefinition) {
        /* no op */
    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<>();
        definitionSetList.add(this.create2018AtsLowBetaRampSqueezeOpticsSet());
        definitionSetList.add(this.create2018AtsIonRampSqueezeOpticsSet());
        definitionSetList.add(this.create2018AtsHighTeleRampSqueezeOpticsSet());
        definitionSetList.add(this.create2018HighBetaOptics());
        return definitionSetList;
    }

    /**
     * ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)
     */
    private OpticDefinitionSet create2018AtsLowBetaRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));

        /* ramp and squeeze to 3m in 1,5 and 8 */
        builder.addOptic("R2017a_A11mC11mA10mL10m",
                modelFile("strengths/ats/low-beta/ats_11m_fixQ8L4.madx"),
                modelFile("strengths/R2017a_A11mC11mA10mL10m_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A970C970A10mL970",
                modelFile("strengths/ats/low-beta/ats_970cm.madx"),
                modelFile("strengths/R2017a_A970C970A10mL970_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A920C920A10mL920",
                modelFile("strengths/ats/low-beta/ats_920cm.madx"),
                modelFile("strengths/R2017a_A920C920A10mL920_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A850C850A10mL850",
                modelFile("strengths/ats/low-beta/ats_850cm.madx"),
                modelFile("strengths/R2017a_A850C850A10mL850_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A740C740A10mL740",
                modelFile("strengths/ats/low-beta/ats_740cm.madx"),
                modelFile("strengths/R2017a_A740C740A10mL740_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A630C630A10mL630",
                modelFile("strengths/ats/low-beta/ats_630cm.madx"),
                modelFile("strengths/R2017a_A630C630A10mL630_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A530C530A10mL530",
                modelFile("strengths/ats/low-beta/ats_530cm.madx"),
                modelFile("strengths/R2017a_A530C530A10mL530_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A440C440A10mL440",
                modelFile("strengths/ats/low-beta/ats_440cm.madx"),
                modelFile("strengths/R2017a_A440C440A10mL440_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A360C360A10mL360",
                modelFile("strengths/ats/low-beta/ats_360cm.madx"),
                modelFile("strengths/R2017a_A360C360A10mL360_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A310C310A10mL310",
                modelFile("strengths/ats/low-beta/ats_310cm.madx"),
                modelFile("strengths/R2017a_A310C310A10mL310_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A230C230A10mL300",
                modelFile("strengths/ats/low-beta/ats_230cm.madx"),
                modelFile("strengths/R2017a_A230C230A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A180C180A10mL300",
                modelFile("strengths/ats/low-beta/ats_180cm.madx"),
                modelFile("strengths/R2017a_A180C180A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A135C135A10mL300",
                modelFile("strengths/ats/low-beta/ats_135cm.madx"),
                modelFile("strengths/R2017a_A135C135A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A100C100A10mL300",
                modelFile("strengths/ats/low-beta/ats_100cm.madx"),
                modelFile("strengths/R2017a_A100C100A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A80C80A10mL300",
                modelFile("strengths/ats/low-beta/ats_80cm.madx"),
                modelFile("strengths/R2017a_A80C80A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A65C65A10mL300",
                modelFile("strengths/ats/low-beta/ats_65cm.madx"),
                modelFile("strengths/R2017a_A65C65A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A54C54A10mL300",
                modelFile("strengths/ats/low-beta/ats_54cm.madx"),
                modelFile("strengths/R2017a_A54C54A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A46C46A10mL300",
                modelFile("strengths/ats/low-beta/ats_46cm.madx"),
                modelFile("strengths/R2017a_A46C46A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A40C40A10mL300",
                modelFile("strengths/ats/low-beta/ats_40cm.madx"),
                modelFile("strengths/R2017a_A40C40A10mL300_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A40C40A10mL300_CTPPS1",
                modelFile("strengths/ats/low-beta/ats_40cm_ctpps1.madx"),
                modelFile("strengths/R2017a_A40C40A10mL300_CTPPS1_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017a_A40C40A10mL300_CTPPS2",
                modelFile("strengths/ats/low-beta/ats_40cm_ctpps2.madx"),
                modelFile("strengths/R2017a_A40C40A10mL300_CTPPS2_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017aT_A37C37A10mL300_CTPPS1",
                modelFile("strengths/ats/low-beta/ats_37cm_ctpps1.madx"),
                modelFile("strengths/R2017aT_A37C37A10mL300_CTPPS1_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017aT_A37C37A10mL300_CTPPS2",
                modelFile("strengths/ats/low-beta/ats_37cm_ctpps2.madx"),
                modelFile("strengths/R2017aT_A37C37A10mL300_CTPPS2_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017aT_A33C33A10mL300_CTPPS1",
                modelFile("strengths/ats/low-beta/ats_33cm_ctpps1.madx"),
                modelFile("strengths/R2017aT_A33C33A10mL300_CTPPS1_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017aT_A33C33A10mL300_CTPPS2",
                modelFile("strengths/ats/low-beta/ats_33cm_ctpps2.madx"),
                modelFile("strengths/R2017aT_A33C33A10mL300_CTPPS2_lsaknobs.madx").doNotParse());

        builder.addOptic("R2017aT_A30C30A10mL300_CTPPS2",
                modelFile("strengths/ats/low-beta/ats_30cm_ctpps2.madx"),
                modelFile("strengths/R2017aT_A30C30A10mL300_CTPPS2_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018aT_A27C27A10mL300_CTPPS2",
                modelFile("strengths/ats/low-beta/ats_27cm_ctpps2.madx"),
                modelFile("strengths/R2018aT_A27C27A10mL300_CTPPS2_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018aT_A25C25A10mL300_CTPPS2",
                modelFile("strengths/ats/low-beta/ats_25cm_ctpps2.madx"),
                modelFile("strengths/R2018aT_A25C25A10mL300_CTPPS2_lsaknobs.madx").doNotParse());

        return builder.build();
    }

    /**
     * ATS ramp and squeeze for 2018 ions
     */
    private OpticDefinitionSet create2018AtsIonRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));

        builder.addOptic("R2018i_A970C970A970L970",
                modelFile("strengths/ats/ions/ats-ion_970cm.madx"),
                modelFile("strengths/R2018i_A970C970A970L970_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A920C920A920L920",
                modelFile("strengths/ats/ions/ats-ion_920cm.madx"),
                modelFile("strengths/R2018i_A920C920A920L920_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A850C850A850L850",
                modelFile("strengths/ats/ions/ats-ion_850cm.madx"),
                modelFile("strengths/R2018i_A850C850A850L850_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A760C760A760L760",
                modelFile("strengths/ats/ions/ats-ion_760cm.madx"),
                modelFile("strengths/R2018i_A760C760A760L760_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A670C670A670L670",
                modelFile("strengths/ats/ions/ats-ion_670cm.madx"),
                modelFile("strengths/R2018i_A670C670A670L670_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A590C590A590L590",
                modelFile("strengths/ats/ions/ats-ion_590cm.madx"),
                modelFile("strengths/R2018i_A590C590A590L590_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A520C520A520L520",
                modelFile("strengths/ats/ions/ats-ion_520cm.madx"),
                modelFile("strengths/R2018i_A520C520A520L520_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A450C450A450L450",
                modelFile("strengths/ats/ions/ats-ion_450cm.madx"),
                modelFile("strengths/R2018i_A450C450A450L450_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A400C400A400L400",
                modelFile("strengths/ats/ions/ats-ion_400cm.madx"),
                modelFile("strengths/R2018i_A400C400A400L400_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A360C360A360L360",
                modelFile("strengths/ats/ions/ats-ion_360cm.madx"),
                modelFile("strengths/R2018i_A360C360A360L360_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A320C320A320L320",
                modelFile("strengths/ats/ions/ats-ion_320cm.madx"),
                modelFile("strengths/R2018i_A320C320A320L320_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A290C290A290L290",
                modelFile("strengths/ats/ions/ats-ion_290cm.madx"),
                modelFile("strengths/R2018i_A290C290A290L290_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A230C230A230L230",
                modelFile("strengths/ats/ions/ats-ion_230cm.madx"),
                modelFile("strengths/R2018i_A230C230A230L230_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A185C185A185L185",
                modelFile("strengths/ats/ions/ats-ion_185cm.madx"),
                modelFile("strengths/R2018i_A185C185A185L185_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A135C135A135L150",
                modelFile("strengths/ats/ions/ats-ion_135cm.madx"),
                modelFile("strengths/R2018i_A135C135A135L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A100C100A100L150",
                modelFile("strengths/ats/ions/ats-ion_100cm.madx"),
                modelFile("strengths/R2018i_A100C100A100L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A82C82A82L150",
                modelFile("strengths/ats/ions/ats-ion_82cm.madx"),
                modelFile("strengths/R2018i_A82C82A82L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A68C68A68L150",
                modelFile("strengths/ats/ions/ats-ion_68cm.madx"),
                modelFile("strengths/R2018i_A68C68A68L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A57C57A57L150",
                modelFile("strengths/ats/ions/ats-ion_57cm.madx"),
                modelFile("strengths/R2018i_A57C57A57L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A50C50A50L150",
                modelFile("strengths/ats/ions/ats-ion_50cm.madx"),
                modelFile("strengths/R2018i_A50C50A50L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A44C44A50L150",
                modelFile("strengths/ats/ions/ats-ion_44cm.madx"),
                modelFile("strengths/R2018i_A44C44A50L150_lsaknobs.madx").doNotParse());

        builder.addOptic("R2018i_A40C40A50L150",
                modelFile("strengths/ats/ions/ats-ion_40cm.madx"),
                modelFile("strengths/R2018i_A40C40A50L150_lsaknobs.madx").doNotParse());

        return builder.build();
    }


    /**
     * ATS high telescope index ramp and squeeze for 2018 MDs
     */
    private OpticDefinitionSet create2018AtsHighTeleRampSqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));

        builder.addOptic("R2018a_A200C200A10mL300", modelFile("strengths/ATS_RoundHighTele/200cm.madx"));

        builder.addOptic("R2018aT200_A182C182A10mL300", modelFile("strengths/ATS_RoundHighTele/182cm.madx"));

        builder.addOptic("R2018aT200_A155C155A10mL300", modelFile("strengths/ATS_RoundHighTele/155cm.madx"));

        builder.addOptic("R2018aT200_A122C122A10mL300", modelFile("strengths/ATS_RoundHighTele/122cm.madx"));

        builder.addOptic("R2018aT200_A95C95A10mL300", modelFile("strengths/ATS_RoundHighTele/95cm.madx"));

        builder.addOptic("R2018aT200_A77C77A10mL300", modelFile("strengths/ATS_RoundHighTele/77cm.madx"));

        builder.addOptic("R2018aT200_A65C65A10mL300", modelFile("strengths/ATS_RoundHighTele/65cm.madx"));

        builder.addOptic("R2018aT172_A56C56A10mL300", modelFile("strengths/ATS_RoundHighTele/56cm.madx"));

        builder.addOptic("R2018aT145_A47C47A10mL300", modelFile("strengths/ATS_RoundHighTele/47cm.madx"));

        builder.addOptic("R2018aT123_A40C40A10mL300", modelFile("strengths/ATS_RoundHighTele/40cm.madx"));

        builder.addOptic("R2018aT105_A34C34A10mL300", modelFile("strengths/ATS_RoundHighTele/34cm.madx"));

        builder.addOptic("R2018aT92_A30C30A10mL300", modelFile("strengths/ATS_RoundHighTele/30cm.madx"));

        builder.addOptic("R2018aT83_A27C27A10mL300", modelFile("strengths/ATS_RoundHighTele/27cm.madx"));

        builder.addOptic("R2018aT77_A25C25A10mL300", modelFile("strengths/ATS_RoundHighTele/25cm.madx"));

        return builder.build();
    }


    private OpticDefinitionSet create2018HighBetaOptics() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("strengths/ats/low-beta/ats_11m_fixQ8L4.madx"));

        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-tune-coll-rq-ats.madx").doNotParse());
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-tune-coll-ats.madx").doNotParse());
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-chroma-ats.madx").doNotParse());


        /* squeeze sequence down to 90 m in all 1+5 */
        builder.addOptic(
                "R2018h_A12mC12mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0012.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0012.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0012_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0012_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.12m.madx"));

        builder.addOptic(
                "R2018h_A14mC14mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0014.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0014.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0014_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0014_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A16mC16mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0016.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0016.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0016_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0016_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A19mC19mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0019.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0019.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0019_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0019_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A22mC22mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0022.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0022.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0022_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0022_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A25mC25mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0025.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0025.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0025_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0025_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A30mC30mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0030.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0030.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0030_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0030_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A33mC33mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0033.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0033.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0033_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0033_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A36mC36mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0036.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0036.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0036_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0036_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A40mC40mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0040.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0040.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0040_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0040_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A43mC43mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0043.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0043.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0043_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0043_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A46mC46mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0046.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0046.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0046_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0046_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A51mC51mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0051.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0051.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0051_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0051_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A54mC54mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0054.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0054.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0054_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0054_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A60mC60mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0060.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0060.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0060_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0060_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A67mC67mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0067.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0067.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0067_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0067_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A75mC75mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0075.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0075.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0075_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0075_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A82mC82mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0082.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0082.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0082_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0082_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        builder.addOptic(
                "R2018h_A90mC90mA10mL10m",
                modelFile("strengths/highbeta90m/IR1/IP1_0090.madx"),
                modelFile("strengths/highbeta90m/IR5/IP5_0090.madx"),
                modelFile("strengths/highbeta90m/IR1/IP1_0090_bump.str"),
                modelFile("strengths/highbeta90m/IR5/IP5_0090_bump.str"),
                modelFile("strengths/highbeta90m/mq-cor.madx"));

        return builder.build();
    }


}