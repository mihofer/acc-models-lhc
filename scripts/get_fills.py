import pytimber
import pjlsa

lsa=pjlsa.LSAClient()

data=lsa.findBeamProcessHistory("2018-01-01 00:00:00","2019-01-01 00:00:00")


fh=open("scenarios/fills.txt",'w')
for k,v in sorted(data.items()):
     for ts,n in v:
       print(k,pytimber.dumpdate(ts,fmt='%Y-%m-%dT%H:%M:%S',zone='utc'),n,file=fh)

fh.close()


