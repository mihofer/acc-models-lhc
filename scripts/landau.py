import pjlsa
from cpymad.madx import Madx
from glob import glob
from pathlib import Path

def str2mad(lsa,k):
    kk=k.split('/')[0]
    return lsa.findMadStrengthNameByPCName(kk)

def mkknob(lsa,optic,lsaname,madname):
    out=[]
    out.append(f"! start {lsaname}")
    try:
      fct=lsa.getKnobFactors(lsaname,optic)
      for kk,vv in fct.items():
         madv=str2mad(lsa,kk)
         out.append(f"add2expr,var={madv},expr={vv}*{madname};")
      out.append(f"! end {lsaname}\n")
      return "\n".join(out)
    except:
        return ""

optics=[ Path(ff).stem for ff in glob("operation/optics/*")]

knobs={ 'LHCBEAM1/LANDAU_DAMPING_RUN2':'on_lob1',
        'LHCBEAM2/LANDAU DAMPING':'on_lob2',
        'LHCBEAM1/LANDAU_DAMPING_STRONG':'on_losb1',
        'LHCBEAM2/LANDAU_DAMPING_STRONG':'on_losb2',
        'LHCBEAM1/LANDAU_DAMPING_WEAK':'on_lowb1',
        'LHCBEAM2/LANDAU_DAMPING_WEAK':'on_lowb2' }


lsa=pjlsa.LSAClient()
for optic in optics:
    print(optic)
    with open(f"strengths/{optic}_landau.madx","w") as fh:
        for lsaname,madname in knobs.items():
            knob=mkknob(lsa,optic,lsaname,madname)
            if knob!="":
               print(lsaname,madname)
               fh.write(knob)


