/*
* Installs the AC dipole into the LHC sequence, subsequent twiss calls
* will take the AC dipoles effects into account.
* @param natural_qx: Fractional part of the natural horizontal tune.
* @param natural_qy: Fractional part of the natural vertical tune.
* @param driven_qx: Fractional part of the driven horizontal tune.
* @param driven_qy: Fractional part of the driven vertical tune.
* @param beam_number: LHC beam number, either 1 or 2.
*/
install_ac_dipole_as_matrix(natural_qx, natural_qy, driven_qx, driven_qy, beam_number): macro = {
    Qx = natural_qx;
    Qxd = driven_qx;
    
    Qy = natural_qy;
    Qyd = driven_qy;

    use, period=LHCBbeam_number;
    twiss;
    
    betxac=table(twiss, MKQA.6L4.Bbeam_number, BEAM, betx);
    betyac=table(twiss, MKQA.6L4.Bbeam_number, BEAM, bety);

    hacmap21=2*(cos(2*pi*Qxd)-cos(2*pi*Qx))/(betxac*sin(2*pi*Qx));
    vacmap43=2*(cos(2*pi*Qyd)-cos(2*pi*Qy))/(betyac*sin(2*pi*Qy));
    
    hacmap: matrix, l=0, rm21=hacmap21;
    vacmap: matrix, l=0, rm43=vacmap43;
    
    seqedit, sequence=LHCBbeam_number;
        flatten;
        install, element=hacmap, at=1.583/2, from=MKQA.6L4.Bbeam_number;
        install, element=vacmap, at=1.583/2, from=MKQA.6L4.Bbeam_number;
    endedit;

    use, period=LHCBbeam_number;
}



/*
* Installs the AC dipole as an element to use with MAD-X track 
* into the LHC sequence.
* @param n_qx: Fractional part of the natural horizontal tune.
* @param n_qy: Fractional part of the natural vertical tune.
* @param d_qx: Fractional part of the driven horizontal tune.
* @param d_qy: Fractional part of the driven vertical tune.
* @param beam_number: LHC beam number, either 1 or 2.
*/
install_acd_as_element(n_qx, n_qy, d_qx, d_qy, beam_number, r1, r2, r3, r4): macro = {
    pbeam = beam%lhcbbeam_number->pc;
    betxac=table(twiss, MKQA.6L4.Bbeam_number, BEAM, betx);
    betyac=table(twiss, MKQA.6L4.Bbeam_number, BEAM, bety);

    Qx = n_qx;
    Qy = n_qy;

    Qxd = d_qx;
    Qyd = d_qy;

    ampx = 1;    !-- unit: [mm]
    ampy = 1;    !-- unit: [mm]
    MKACH.6L4.Bbeam_number: hacdipole, l=0, freq=Qxd, lag=0,
        volt=0.042*pbeam*abs(Qxd-Qx)/sqrt(180.0*betxac)*ampx,
        ramp1=r1, ramp2=r2, ramp3=r3, ramp4=r4;
    MKACV.6L4.Bbeam_number: vacdipole, l=0, freq=Qyd, lag=0,
        volt=0.042*pbeam*abs(Qyd-Qy)/sqrt(177.0*betyac)*ampy,
        ramp1=r1, ramp2=r2, ramp3=r3, ramp4=r4;

    seqedit, sequence=LHCBbeam_number;
        flatten;
        install, element=MKACH.6L4.Bbeam_number, at=0.0, from=MKQA.6L4.Bbeam_number;
        install, element=MKACV.6L4.Bbeam_number, at=0.0, from=MKQA.6L4.Bbeam_number;
    endedit;

    use, period=LHCBbeam_number;
}